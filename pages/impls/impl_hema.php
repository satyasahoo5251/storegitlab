<?php session_start(); 
/* Starts the session */

if(!isset($_SESSION['UserData']['Username'])){
	header("location:../login.php");
	exit;
}
?>
<!doctype html>
<html lang="en">

<head>
    <title>Hema - Implants Usage</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://unpkg.com/tabulator-tables@4.4.1//dist/css/tabulator_simple.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


    <link rel="stylesheet" href="../../assets/css/custom.css">



</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-success">
        <a name="" id="" class="btn btn-primary btn-sm btn-2" href="../hema.php" role="button">Hema N Clear</a>

    </nav>


    <div class="container cont">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ml-7 mt-3 pt-2 pb-2">
            <h3>Hema N Clear Implants Usage Details</h3>

        </div>

        <div class="row mt-2">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="hema_implusage" class=""></div>

            </div>
        </div>


    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
    <!-- <script src="../../vendors/jquery/dist/jquery.min.js"></script> -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!-- Tabulator Pugin -->
    <script type="text/javascript" src="https://unpkg.com/tabulator-tables@4.4.1/dist/js/tabulator.min.js"></script>
    <!-- Validator Plugin -->
    <!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script> -->
    <!-- Our Code -->
    <script src="../../assets/js/impl/impl_hema.js"></script>


</body>

</html>