<?php session_start(); 
/* Starts the session */

if(!isset($_SESSION['UserData']['Username'])){
	header("location:../login.php");
	exit;
}
?>
<!doctype html>
<html lang="en">

<head>
    <title>Smith n Nephew - Add Surgery</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


    <link rel="stylesheet" href="../assets/css/custom.css">



</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-success">
        <a name="" id="" class="btn btn-primary btn-sm btn-2" href="./smith.php" role="button">Smith n Nephew</a>
        <span class="offset-4" style="color: white;">Welcome <a style="color: white;" id="username"><?php echo $_SESSION['Username'];?></a></span>
    </nav>


    <div class="container cont">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ml-7 mt-3 pt-2 pb-2">
            <h3>Smith n Nephew Add Surgery Details</h3>
            <form id="add_bio" class=" pt-2 pb-3">
                <div class="form-row">
                    <div class="form-row pl-3 col-md-12">
                        <div class="form-group col-3">
                            <input type="text" name="" id="hname" class="form-control" placeholder="Hospital name"
                                aria-describedby="helpId" required="required">

                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" id="dname" class="form-control" placeholder="Surgeon name"
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-2">
                            <input type="text" name="" id="pname" class="form-control" placeholder="Patient name"
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-2">
                            <input type="text" name="" id="ipnum" class="form-control" placeholder="IP Number"
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-2" id='datetimepicker1'>
                            <!-- <input type="text" name="" id="dosu" class="form-control" placeholder="Date"
                                aria-describedby="helpId" required> -->
                            <input type="text" id="dosu" class="form-control from datepicker" placeholder="Date"
                                name="from" aria-describedby="helpId" required="required">
                        </div>
                    </div>
                    <div class="form-group col-3 ml-3">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="" id="bilateral"
                                    value="checkedValue">
                                Bi-Latertal
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-3 ml-3">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="" id="navigation"
                                    value="checkedValue">
                                Navigation
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row-container bg-lighter-gray mt-2 pt-3 pb-3 bg-radius-7 cont mt-cust">
                    <h4 class="pl-3">Used Implant Details :</h4>
                    <div class="impls form-row pl-3 pr-3 mt-1">
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="Implant Name/Number"
                                aria-describedby="helpId" required="required">

                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="Batch No."
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" class="form-control to datepicker2" placeholder="Expire Date"
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="DP price"
                                aria-describedby="helpId" readonly required="required">
                        </div>


                    </div>
                    <div class="impls form-row pl-3 pr-3">

                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="Implant Name/Number"
                                aria-describedby="helpId" required="required">

                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="Batch No."
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" class="form-control to datepicker2" placeholder="Expire Date"
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="DP price"
                                aria-describedby="helpId" readonly required="required">
                        </div>

                    </div>
                    <div class="impls form-row pl-3 pr-3">

                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="Implant Name/Number"
                                aria-describedby="helpId" required="required">

                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="Batch No."
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" class="form-control to datepicker2" placeholder="Expire Date"
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="DP price"
                                aria-describedby="helpId" readonly required="required">
                        </div>

                    </div>
                    <div class="form-row ml-1 pl-3 pr-3">
                        <!-- <div class="pl-3 ml-c"> -->
                        <a name="" id="add_row_impl" class="btn btn-primary btn-md" href="#" role="button">Add
                            Row</a>
                    </div>
                </div>

                <!-- for evolutis implants -->

                <div class="row-container bg-lighter-gray mt-2 pt-3 pb-3 bg-radius-7 cont mt-cust">
                    <div class="form-row ml-1 pl-3 pr-3">
                        <a name="" id="add_evo_impls" class="btn text-warning" href="#" role="button">Add
                            Evolutis implants</a>
                    </div>
                </div>


                <!-- This iss the second sec -->
                <div class="row-container bg-lighter-gray mt-2 pt-3 pb-3 bg-radius-7 cont mt-cust">
                    <h4 class="ml-3">Consumables Details :</h4>
                    <div class="consumes form-row pl-3 pr-3 mt-c">
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="Consumables"
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="QTY"
                                aria-describedby="helpId" required="required">
                        </div>
                        <div class="form-group col-3">
                            <!-- <input type="text" name="" id="" class="form-control" placeholder="Consumables"
                                    aria-describedby="helpId"> -->

                        </div>
                        <div class="form-group col-3">
                            <!-- <input type="text" name="" id="" class="form-control" placeholder="QTY"
                                    aria-describedby="helpId"> -->
                        </div>

                    </div>
                    <div class="consumes form-row pl-3 pr-3">
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="Consumables"
                                aria-describedby="helpId">
                        </div>
                        <div class="form-group col-3">
                            <input type="text" name="" id="" class="form-control" placeholder="QTY"
                                aria-describedby="helpId">
                        </div>
                        <div class="form-group col-3">
                            <!-- <input type="text" name="" id="" class="form-control" placeholder="Consumables"
                                    aria-describedby="helpId"> -->
                        </div>
                        <div class="form-group col-3">
                            <!-- <input type="text" name="" id="" class="form-control" placeholder="QTY"
                                    aria-describedby="helpId"> -->
                        </div>

                    </div>

                    <div class="form-row  ml-1 pl-3 pr-3">
                        <a name="" id="add_row_con" class="btn btn-primary btn-md" href="#" role="button">Add Row</a>
                    </div>
                </div>
                <div class="mt-2">
                    <button type="submit" id="add_smith" class="btn btn-success">Add Surgery Usage</button>

                </div>

            </form>
        </div>


    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
    <!-- <script src="../vendors/jquery/dist/jquery.min.js"></script> -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="https://smtpjs.com/v3/smtp.js"></script>
    <script src="../assets/js/add_smith.js"></script>
    <script src="../vendors/moment/min/moment.min.js"></script>

</body>

</html>