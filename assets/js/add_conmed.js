$(document).ready(function () {
    var hospitals = [];
    var doctors = [];
    var prlist = [];
    var evoprlist = [];
    var consumables = [];
    var evocount = 0;

    $.ajax({
        type: "get",
        url: "https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/hospitals?apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi",
        success: function (response) {
            $.each(response, function (i, item) {
                hospitals.push(item.name);
            });
            // console.log(hospitals);
        },
        fail: (d, s, e) => {
            console.log(d + " " + s + " " + e);
        }
    });
    $.ajax({
        type: "get",
        url: "https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/doctors?apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi",
        success: function (response) {
            $.each(response, function (i, item) {
                doctors.push(item.name);
            });
            // console.log(doctors);
        },
        fail: (d, s, e) => {
            console.log(d + " " + s + " " + e);
        }
    });
    // Start For row 1
    $.ajax({
        type: "get",
        url: "https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/conmed?apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi",
        success: function (response) {
            // console.log(response);
            $.each(response, function (i, item) {
                // console.log(item.prcode);
                prlist.push(item.prname + "_" + item.prcode);
            });
            // console.log(prlist);
        },
        fail: (d, s, e) => {
            console.log(d + " " + s + " " + e);
        }
    });

    $.ajax({
        type: "get",
        url: "https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/conmed?apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi",
        success: function (response) {
            // console.log(response);
            $.each(response, function (i, item) {
                // console.log(item.prcode);
                evoprlist.push(item.prname + "_" + item.prcode);
            });
            // console.log(prlist);
        },
        fail: (d, s, e) => {
            console.log(d + " " + s + " " + e);
        }
    });

    $.ajax({
        type: "get",
        url: "https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/consumables?apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi",
        success: function (response) {
            // console.log(response);
            $.each(response, function (i, item) {
                // console.log(item.prcode);
                consumables.push(item.cnsname);
            });
            // console.log(prlist);
        },
        fail: (d, s, e) => {
            console.log(d + " " + s + " " + e);
        }
    });

    // Start for row 1
    $("#hname").autocomplete({
        source: hospitals,
        minLength: 2,
        delay: 1000
    });
    $("#dname").autocomplete({
        source: doctors,
        minLength: 2,
        delay: 1000
    });

    $("div.impls.form-row.pl-3.pr-3>div:nth-child(1)>input").autocomplete({
        source: prlist,
        minLength: 2,
        delay: 1000
    });

    $("div.evoimpls > input").autocomplete({
        source: evoprlist,
        minLength: 2,
        delay: 1000
    });

    $("div>div.consumes.form-row.pl-3.pr-3>div:nth-child(1)>input").autocomplete({
        source: consumables,
        minLength: 1,
        // delay: 1000
    });

    $("div.impls.form-row.pl-3.pr-3>div:nth-child(1)>input").focusout((e) => {

        var sprcode = e.currentTarget.value.split("_")[1];
        var sprice;
        $.ajax({
            type: "get",
            url: 'https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/conmed?q={"prcode":"' + sprcode + '"}&fo=true&apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi',
            success: function (response) {
                sprice = response.dpvalue;
                e.currentTarget.parentElement.parentElement.childNodes[7].childNodes[1].value = sprice;
            },
            fail: (d, s, e) => {
                console.log(d + " " + s + " " + e);
            }
        });
    });

    $("#add_row_impl").click(function (e) {
        // e.preventDefault();

        $(this).parent().prev().clone(true, false).insertBefore($(this).parent());
        // console.log("Clonnig.....");
        // var div = $(this).parent().prev().children()[2];
        // var str1 = "<div class='form-group col-3'><input type='text' class='form-control to datepicker' placeholder='Expire Date'></div><div class='form-group col-3'><input type='text' name='' class='form-control' placeholder='DP price' readonly></div>";
        $(this).parent().prev().children().slice(-2).remove();

        var div1 = document.createElement("div");
        div1.setAttribute("class", "form-group col-3");
        var expdate = document.createElement("input");
        expdate.setAttribute("class", "form-control to datepicker2");
        expdate.setAttribute("placeholder", "Expire Date");
        expdate.setAttribute("required", "required");
        div1.append(expdate);
        var dpele = document.createElement("input");
        dpele.setAttribute("class", "form-control");
        dpele.setAttribute("placeholder", "DP Value");
        dpele.setAttribute("readonly", "true");
        dpele.setAttribute("required", "required");
        var div2 = document.createElement("div");
        div2.setAttribute("class", "form-group col-3")
        div2.append(dpele);
        // console.log(div2);
        $(this).parent().prev().append(div1);
        $(this).parent().prev().append(div2);

        // console.log(div);

        // console.log($(this).parent().prev());
        $("div.impls.form-row.pl-3.pr-3>div:nth-child(1)>input").autocomplete({
            source: prlist,
            minLength: 2,
            delay: 1000
        });

        $("div.impls.form-row.pl-3.pr-3>div:nth-child(1)>input").focusout((e) => {

            var sprcode = e.currentTarget.value.split("_")[1];
            console.log(sprcode);
            var sprice;
            $.ajax({
                type: "get",
                url: 'https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/conmed?q={"prcode":"' + sprcode + '"}&fo=true&apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi',
                success: function (response) {
                    sprice = response.dpvalue;
                    // console.log("sprice" + sprice);
                    if (e.currentTarget.parentElement.parentElement.childNodes[8].childNodes[0]) {
                        e.currentTarget.parentElement.parentElement.childNodes[8].childNodes[0].value = sprice;
                    }

                },
                fail: (d, s, e) => {
                    console.log(d + " " + s + " " + e);
                }
            });
        });

    });

    // adding EVOLUTIS Implants in conmed ::::::
    $("#add_evo_impls").click(function (e) {
        e.preventDefault();

        if (evocount < 2) {
            console.log("Creating........");
            // $(this).parent().prev().children().slice(-4).remove();
            var div0 = document.createElement("div");
            div0.setAttribute("class", "form-row pl-3 pr-3 mt-1 evoset");

            var div1 = document.createElement("div");
            div1.setAttribute("class", "form-group col-3  evoimpls");
            var div2 = document.createElement("div");
            div2.setAttribute("class", "form-group col-3")
            var div3 = document.createElement("div");
            div3.setAttribute("class", "form-group col-3");
            var div4 = document.createElement("div");
            div4.setAttribute("class", "form-group col-3")

            var evimpl = document.createElement("input");
            evimpl.setAttribute("class", "form-control border-warning");
            evimpl.setAttribute("placeholder", "Evolutis Implant");
            evimpl.setAttribute("required", "required");
            // evimpl.setAttribute("autocomplete", "off");
            div1.append(evimpl);

            var evbno = document.createElement("input");
            evbno.setAttribute("class", "form-control border-warning");
            evbno.setAttribute("placeholder", "Batch No");
            evbno.setAttribute("required", "required");
            // evbno.setAttribute("autocomplete", "off");
            div2.append(evbno);

            var expdate = document.createElement("input");
            expdate.setAttribute("class", "form-control to datepicker2 border-warning");
            expdate.setAttribute("placeholder", "Expire Date");
            expdate.setAttribute("required", "required");
            // expdate.setAttribute("autocomplete", "off");

            div3.append(expdate);

            var dpele = document.createElement("input");
            dpele.setAttribute("class", "form-control border-warning");
            dpele.setAttribute("placeholder", "DP Value");
            dpele.setAttribute("readonly", "true");
            dpele.setAttribute("required", "required");
            div4.append(dpele);

            div0.append(div1);
            div0.append(div2);
            div0.append(div3);
            div0.append(div4);

            $($(this).parent().parent()).prepend(div0);

            $("div.evoimpls > input").autocomplete({
                source: evoprlist,
                minLength: 2,
                delay: 1000
            });

            $("div.evoimpls > input").focusout((e) => {

                var sprcode = e.currentTarget.value.split("_")[1];
                console.log(sprcode);
                var sprice;
                $.ajax({
                    type: "get",
                    url: 'https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/conmed?q={"prcode":"' + sprcode + '"}&fo=true&apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi',
                    success: function (response) {
                        sprice = response.dpvalue;

                        if (e.currentTarget.parentElement.parentElement.childNodes[3].childNodes[0]) {
                            e.currentTarget.parentElement.parentElement.childNodes[3].childNodes[0].value = sprice;
                        }

                    },
                    fail: (d, s, e) => {
                        console.log(d + " " + s + " " + e);
                    }
                });
            });

            evocount++;

        }

    });

    $("#add_row_con").click(function (e) {
        // e.preventDefault();
        $(this).parent().prev().clone(true, false).insertBefore($(this).parent());
        // console.log($(this).parent().prev());
        $("div>div.consumes.form-row.pl-3.pr-3>div:nth-child(1)>input").autocomplete({
            source: consumables,
            minLength: 1,
            // delay: 1000
        });
    });


    $("#add_bio").validate({

        // rules: {},
        showErrors: function (errorMap, errorList) {},
        onfocusout: false,
        onkeyup: false,

        submitHandler: function (form) {

            var temp = $("div.impls.form-row.pl-3.pr-3>div>input").toArray();
            var temp2 = [];
            var temp3 = $("div.consumes.form-row.pl-3.pr-3>div>input").toArray();
            var temp4 = [];

            var evoimpls = $("div.evoset > div.form-group > input").toArray();
            // var evoimplsvals = [];

            var surusage = [];
            var consusage = [];
            var totalDp = 0;

            $.each(temp, function (i, data) {
                if (data['value'] !== "")
                    temp2.push(data['value'])
            });
            $.each(evoimpls, (i, data) => {
                temp2.push(data['value']);
            });
            $.each(temp3, function (i, data) {
                if (data['value'] !== "")
                    temp4.push(data['value'])
            });

            Object.defineProperty(Array.prototype, 'chunk', {
                value: function (chunkSize) {
                    var R = [];
                    for (var i = 0; i < this.length; i += chunkSize)
                        R.push(this.slice(i, i + chunkSize));
                    return R;
                }
            });

            temp2 = temp2.chunk(4);

            temp4 = temp4.chunk(2);

            for (var i = 0; i < temp2.length; i++) {

                var usg3 = {
                    implname: temp2[i][0],
                    implbno: temp2[i][1],
                    impleexp: temp2[i][2],
                    impldpv: temp2[i][3]
                }
                totalDp = totalDp + Number(usg3.impldpv);
                surusage.push(usg3);
            }

            for (var i = 0; i < temp4.length; i++) {

                var cusg = {
                    cnsname: temp4[i][0],
                    cnsqty: temp4[i][1]
                }
                consusage.push(cusg);
            }

            var surgery = JSON.stringify({
                Date_Created: new Date(),
                Surgery_Date: $("#dosu").val(),
                Principle: "Conmed",
                Hospital: $("#hname").val(),
                Surgeon: $("#dname").val(),
                Pateint: $("#pname").val(),
                Ip_number: $("#ipnum").val(),
                Total_DP: totalDp,
                Bilateral: ($("#bilateral").is(':checked')) ? true : false,
                allocated: null,
                closed: null,
                invReceived: null,
                ackInvReceived: null,
                case: null,
                category: null,
                navigation: null,
                Approved: false,
                invoiceVal: 0,
                xCharges: 0,
                balance: 0,
                createdby: $('#username').text(),
                usage: surusage,
                conusage: consusage
            });

            // console.log(surgery);

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/surgery-conmed?apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi",
                data: surgery,
                success: function (respond) {
                    var SurgObj = JSON.parse(surgery)
                    var impls = [];
                    var consar = [];

                    $(SurgObj.usage).each((i, data) => {
                        impls.push(data.implname.split("_")[0]);
                    });
                    $(SurgObj.conusage).each((i, data) => {
                        consar.push(data.cnsname + " - " + data.cnsqty + " no(s)");
                    });
                    Email.send({
                        SecureToken: "0acc204b-1061-42b3-a465-c59e23d2feae",
                        To: ['info@rightavenue.co.in', 'vara@rainfo.in'],
                        From: "surgery.rae@gmail.com",
                        Subject: "conmed",
                        Body:

                            "<h2>New Surgery Details</h2>" +
                            // "Date_Created " + ": " + SurgObj.Date_Created + "<br>"+
                            "Surgery Date " + ": " + SurgObj.Surgery_Date + "<br>" +
                            "Principle " + ": " + SurgObj.Principle + "<br>" +
                            "Hospital " + ": " + SurgObj.Hospital + "<br>" +
                            "Surgeon " + ": " + SurgObj.Surgeon + "<br>" +
                            "Pateint " + ": " + SurgObj.Pateint + "<br>" +
                            "IP number " + ": " + SurgObj.Ip_number + "<br>" +
                            "Total DP Value " + ": " + SurgObj.Total_DP + "<br>" +
                            // "invoiceVal " + ": " + SurgObj.invoiceVal + "<br>"
                            // "xCharges " + ": " + SurgObj.xCharges + "<br>"
                            // "balance " + ": " + SurgObj.balance + "<br>"
                            "<h3>Implants Usage Details</h3>" +
                            impls.join("<br>") +
                            // // conusage 

                            "<h3>Consumables Usage Details</h3>" +
                            consar.join("<br>")

                    }).then(
                        //   message => alert("Mail Sent SuccessFully."),   
                        function pageRedirect() {
                            var delay = 500;

                            setTimeout(function () {
                                window.location = "../pages/conmed.php";
                            }, delay);

                        }
                    );
                    // alert("your data successfully submit");
                    // window.location = "../index.php";

                }
            });

            return false;

        }
    });


    $('body').on('focus', ".datepicker", function () {

        if ($(this).hasClass('hasDatepicker') === false) {
            var dateToday = new Date();
            $(this).datepicker({
                defaultDate: "-1d",
                showOnFocus: true,
                changeMonth: true,
                showAnim: "slideDown",
                numberOfMonths: 1,
                minDate: "-4D",
                maxDate: dateToday,
                yearRange: "2000:2030",
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'dd/mm/yy'
            });
        }

    });
    $('body').on('focus', ".datepicker2", function () {

        if ($(this).hasClass('hasDatepicker') === false) {
            var dateToday = new Date();
            $(this).datepicker({
                defaultDate: "-1d",
                showOnFocus: true,
                changeMonth: true,
                showAnim: "slideDown",
                numberOfMonths: 1,
                minDate: "+1M",
                // maxDate: dateToday,
                yearRange: "2000:2030",
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'dd/mm/yy'
            });
        }

    });

    $("input").focusout(function () {

        if ($(this).val() == '') {
            $(this).attr('class', 'border-danger form-control');
            // alert($(this));
        } else {

            $(this).attr('class', 'form-control');
            $(this).css('border', 'solid 2px green');
        }
    });

});
// End 