$(document).ready(function () {
    var curid;
    var curDpval = 0;

    //Build Tabulator
    var table = new Tabulator("#smith_usagetable", {
        height: "100%",
        groupToggleElement: "header",
        groupBy: (data) => {

            return data.Surgery_Date.split("/")[1] + "/" + data.Surgery_Date.split("/")[2];
        },
        groupHeader: function (value, count, data, group) {
            var tempcount = 0;

            $(data).each((i, data2) => {
                if (data2.Bilateral)
                    tempcount += 1;
            });

            var month = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

            var totalcases = count + tempcount;

            var mval = Number(value.split("/")[0]);
            var yval = value.split("/")[1];
            return month[mval] + " - " + yval + " <span style='color:green; margin-left:10px;'>( " + totalcases + " Total Cases (inc. Bilateral:" + tempcount + ") )</span>";
        },
        groupStartOpen: function (value, count, data, group) {

            var cmonth = new Date().getUTCMonth();
            var cyear = new Date().getFullYear();
            cmonth += 1;
            var currmy = cmonth + "/" + cyear;

            // console.log(group);
            console.log(group._group.key);


            return group._group.key == currmy;
        },
        // groupBy : "9/2019",
        layout: "fitColumns",
        ajaxURL: "https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/surgery-snnephew?s={'Surgery_Date':-1}&apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi",
        // ajaxProgressiveLoad: "scroll",
        paginationSize: 20,
        placeholder: "No Data",
        rowFormatter: function (row) {
            if (row.getData().Bilateral == true) {
                row.getElement().style.backgroundColor = "#F0E68C";
                // console.log(row);
            }
        },
        // data: "nestedData",
        // selectable: true,
        rowClick: function (e, row) {
            const id = row.getData()._id.$oid;
            curid = id;
            var impls = [];
            var cons = [];
            $(row.getData().usage).each((i, data) => {
                impls.push((i + 1) + ") <b>" + data.implname.split("_")[1] + "</b> - " + data.implname.split("_")[0]);
            });
            $(row.getData().conusage).each((i, data) => {
                cons.push((i + 1) + ") " + data.cnsname + " - <b>" + data.cnsqty + "</b> No(s)");
            });
            $("#edithname").text(row.getData().Hospital);
            $("#editdname").text(row.getData().Surgeon);
            $("#editpname").text(row.getData().Pateint);
            $("#editpnum").text(row.getData().Ip_number);
            $("#editsdate").text(row.getData().Surgery_Date);
            $("#editDP").val(row.getData().Total_DP);
            curDpval = row.getData().Total_DP;
            $("#editusage").php("<small><u>Implant Usage:</u><br/> " + impls.join("<br/>") + "<br/><u>Consumable Usage:</u><br/>" + cons.join("<br/>") + "</small>");

            $('#Modal').modal('show');
        },
        initialSort: [{
            column: "Surgery_Date",
            dir: "desc"
        }],
        columns: [{
                formatter: "rownum",
                align: "center",
                width: 40
            },
            {
                title: "ID",
                field: "_id.$oid",
                visible: false,
            },
            {
                title: "Date_Created",
                field: "Date_Created",
                sorter: "date",
                // width: 200,
                headerFilter: "input",
                // dir: 'desc',
                visible: false,
            },
            {
                title: "Date",
                field: "Surgery_Date",
                // sorter: "string",
                // width: 200,
                // headerFilter: "input",
                // dir: 'desc',
                sorter: "date",
                headerFilter: (cell, onRendered, success, cancel, editorParams) => {

                    // console.log(cell);

                    var container = $("<span></span>")
                    //create and style input
                    var start = $("<input type='text' class='datepicker' placeholder='Start'/>");
                    var end = $("<input type='text' class='datepicker' placeholder='End'/>");

                    container.append(start).append(end);

                    var inputs = $("input", container);


                    inputs.css({
                            "padding": "4px",
                            "width": "50%",
                            "box-sizing": "border-box",
                        })
                        .val(cell.getValue());

                    function buildDateString() {
                        return {
                            start: start.val(),
                            end: end.val(),
                        };
                    }

                    //submit new value on blur
                    inputs.on("change blur", function (e) {
                        success(buildDateString());
                    });

                    //submit new value on enter
                    inputs.on("keydown", function (e) {
                        if (e.keyCode == 13) {
                            success(buildDateString());
                        }

                        if (e.keyCode == 27) {
                            cancel();
                        }
                    });
                    // console.log(typeof (container));
                    return container[0];
                },
                headerFilterFunc: (headerValue, rowValue, rowData, filterParams) => {
                    var format = filterParams.format || "DD/MM/YYYY";
                    var start = moment(headerValue.start);
                    var end = moment(headerValue.end);
                    var value = moment(rowValue, format)
                    if (rowValue) {
                        if (start.isValid()) {
                            if (end.isValid()) {
                                // console.log("start-validated");
                                return value >= start && value <= end;
                            } else {
                                // console.log("end-validated");
                                return value >= start;
                            }
                        } else {
                            if (end.isValid()) {
                                return value <= end;
                            }
                        }
                    }
                    $(".subcollc").hide();
                    return false; //must return a boolean, true if it passes the filter.
                }
            },
            {
                title: "Hospital",
                field: "Hospital",
                sorter: "string",
                headerFilter: "input"
                // formatter: "progress"
            },
            {
                title: "Surgeon",
                field: "Surgeon",
                sorter: "string",
                headerFilter: "input"
            },
            {
                title: "Pateint",
                field: "Pateint",
                sorter: "string",
                headerFilter: "input"
                // formatter: "star",
                // align: "center",
                // width: 100
            },
            {
                title: "Ip_number",
                field: "Ip_number",
                sorter: "string",
                headerFilter: "input"
            },
            {
                title: "Total DP Value",
                field: "Total_DP",
                sorter: "number",
                headerFilter: "input"
            },
            {
                title: "Bilateral",
                field: "Bilateral",
                sorter: "boolean",
                formatter: "tickCross",
                align: "center",
                formatterParams: {
                    allowEmpty: true,
                    tickElement: "<i style='color:green' class='fa fa-quote-right'></i>",
                    crossElement: "",
                }
            },
            {
                title: "Approved",
                field: "Approved",
                formatter: "tickCross",
                align: "center",
                formatterParams: {
                    allowEmpty: true,
                    tickElement: "<i style='color:green' class='fa fa-check'></i>",
                    crossElement: "<i style='color:orange' class='fa fa-exclamation-triangle'></i>",
                }
            },
        ],

    });

    $('body').on('focus', ".datepicker", function () {

        if ($(this).hasClass('hasDatepicker') === false) {
            var dateToday = new Date();
            $(this).datepicker({
                // setDate: new Date(),
                // showOnFocus: true,
                // changeMonth: true,
                showAnim: "slideDown",
                // numberOfMonths: 1,
                // minDate: "-4D",
                maxDate: dateToday,
                // yearRange: "2000:2030",
                // changeYear: true,
                // showOtherMonths: true,
                // selectOtherMonths: true,
                // dateFormat: 'dd/mm/yy'
            });
        }

    });

});