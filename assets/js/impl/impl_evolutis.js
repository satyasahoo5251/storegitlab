$().ready(() => {

    totalimpl = [];
    totimpl_JSON = [];
    $.ajax({
        type: "GET",
        url: "https://api.mlab.com/api/1/databases/suregey_tracker_dev/collections/surgery-evolutis?s={'Surgery_Date':-1}&apiKey=GK7Q1vbokviBKtN-W0abEJioaecJxwdi",
        success: function (response) {
            // console.log(response);
            $.each(response, function (i, value) {
                $.each(value.usage, function (j, valuee) {
                    // console.log(valuee.implname);
                    if (valuee.implname != "")
                        totalimpl.push(valuee.implname.split('_')[0]);
                });
            });
            printImpls();
        }
    });

    async function printImpls() {
        // console.log(totalimpl.sort());

        // const used = Object.create(null);
        // totalimpl.forEach(impl => {
        //     used[impl] = true;
        // });
        // console.log(used);

        const counts = Object.create(null);
        totalimpl.forEach(impl => {
            counts[impl] = counts[impl] ? counts[impl] + 1 : 1;
        });

        $.each(counts, function (i, prop) {
            // console.log(i + " " + prop);
            temp = new Object();
            temp.implname = i;
            temp.implcount = prop;
            totimpl_JSON.push(temp)
        });

        console.log(totimpl_JSON);
        drawTable();
    };

    function drawTable() {
        var table = new Tabulator("#evolutis_implusage", {
            height: "600px",
            paginationSize: 20,
            initialSort: [{
                column: "implcount",
                dir: "desc"
            }],
            data: totimpl_JSON, //set initial table data
            columns: [{
                    title: "Sl No",
                    formatter: "rownum",
                    align: "center",
                },
                {
                    title: "Implant",
                    field: "implname",
                    sorter: "string",
                    headerFilter: "input"

                },
                {
                    title: "Usage - Total",
                    field: "implcount",
                    sorter: "number",
                    headerFilter: "input",
                    align: "center",
                },
            ],
        });
    }

});