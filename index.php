<?php session_start(); 
/* Starts the session */
if(!isset($_SESSION['UserData']['Username'])){
	header("location:login.php");
	exit;
}
?>
<!doctype html>
<html lang="en">

<head>
    <title>Biorad</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-print-1.5.6/r-2.2.2/datatables.min.css" /> -->
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
    <link rel="stylesheet" href="./assets/css/custom.css">
    <link href="https://unpkg.com/tabulator-tables@4.4.1/dist/css/tabulator.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/tabulator-tables@4.4.1//dist/css/tabulator_simple.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon">

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-primary">
        <div class="col-sm-8 col-md-offset-3">
            <button id="nav-bio" class="btn btn-sm btn-primary active">Biorad</button>
            <button id="nav-evo" class="btn btn-sm btn-primary"
                onclick="window.location.href='./pages/evolutis.php'">Evolutis</button>
            <button id="nav-snn" class="btn btn-sm btn-primary" onclick="window.location.href='./pages/smith.php'">Smith
                n Nephew</button>
            <button id="nav-con" class="btn btn-sm btn-primary"
                onclick="window.location.href='./pages/conmed.php'">Conmed</button>
            <button id="nav-hem" class="btn btn-sm btn-primary"
                onclick="window.location.href='./pages/hema.php'">HemaClear</button>
        </div>
        <a style="color: white;" id="username">Welcome <?php echo $_SESSION['Username'];?></a>
        <button id="nav-hem" class="btn btn-sm btn-primary offset-2"
            onclick="window.location.href='./logout.php'">Logout</button>
    </nav>
    <div class="container cont">
        <div class="row mt-2 mb-1">

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <h3>Biorad Surgery Tracker</h3>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 offset-4">
                <a name="" id="" class="btn btn-success pull-right" href="./pages/add_biorad.php" role="button">Add
                    Surgery</a>
                <a name="" id="" class="btn btn-info pull-right" href="./pages/impls/impl_biorad.php"
                    role="button">Implants Usage</a>
            </div>
        </div>

        <div id="Modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <!-- <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div> -->
                    <div class="modal-body" id="surgery_edit">

                        <!-- Edit SUregry Details to Update the Invoce Value -->
                        <h4><small>Implant & Consumable Details</small></h4>
                        <form id="upd_inv_frm" class=" pt-2 pb-3">
                            <div class="form-row">
                                <div class="form-row pl-3 col-md-12">
                                    <div class="form-group col-6">
                                        <label id="edithname">hospital</label>
                                    </div>
                                    <div class="form-group col-6">
                                        <label id="editdname">doctor</label>
                                    </div>
                                    <div class="form-group col-5">
                                        <label id="editpname">patient</label>
                                    </div>
                                    <div class="form-group col-4">
                                        <label id="editpnum">IP number</label>
                                    </div>
                                    <div class="form-group col-3">
                                        <label id="editsdate">Suregry Date</label>
                                    </div>
                                    <div class="form-group cal-12">
                                        <label id="editusage"></label>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    </div>
                </div>

            </div>
        </div>

        <div class="row mt-2">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="biorad_usagetable" class=""></div>

            </div>
        </div>


    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script> -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="./vendors/moment/min/moment.min.js"></script>
    <!-- Tabulator Pugin -->
    <script type="text/javascript" src="https://unpkg.com/tabulator-tables@4.4.1/dist/js/tabulator.min.js"></script>
    <!-- Validator Plugin -->
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <!-- Our Code -->
    <script src="./assets/js/biorad.js"></script>


</body>

</html>